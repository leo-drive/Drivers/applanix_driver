#include <gtest/gtest.h>
#include <pcap/pcap.h>

#include <applanix_driver/icd/icd_vector.h>
#include <network/network.h>
#include <network/pcap_reader.h>

TEST(IcdGroupsParsing, group1) {
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("poslv_grp1.pcapng", "172.16.20.12",
                                  5600, network::ProtocolType::UDP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));
  ASSERT_NE(payload.data, nullptr);

  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);
  ASSERT_TRUE(pos_parser.isValid());

  auto it = pos_parser.begin();
  while (it != pos_parser.end() && it->getId() != applanix_driver::icd::group::GROUP_ID_1) {
    ++it; 
  }
  ASSERT_TRUE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x01);
  auto nav_sol = it->as<applanix_driver::icd::group::VehicleNavSolution>();
  ASSERT_EQ(nav_sol.header.id, 0x01);
  ASSERT_EQ(nav_sol.padding, 0);
}

TEST(IcdGroupsParsing, multiGroupRegression) {
  // This pcap file used to crash the driver due to an off-by-two error which happened after
  // parsing group4 which in turn broke the way the subsequent group1 bytes were parsed.
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("poslv_401_4_1.pcap", "192.168.1.10",
                                  5602, network::ProtocolType::TCP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));

  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);
  ASSERT_TRUE(pos_parser.isValid());

  const std::array<applanix_driver::icd::group::Id, 3> expected_ids{
      applanix_driver::icd::group::GROUP_ID_401,
      applanix_driver::icd::group::GROUP_ID_4,
      applanix_driver::icd::group::GROUP_ID_1
  };

  int i = 0;
  for (auto &group : pos_parser) {
    ASSERT_TRUE(group.isSupported());
    ASSERT_EQ(group.getId(), expected_ids[i++]);

    if (group.getId() == applanix_driver::icd::group::GROUP_ID_1) {
      auto nav_sol = group.as<applanix_driver::icd::group::VehicleNavSolution>();
      // Didn't bother reading the decimals, but the point here is that if the seconds past week
      // are good then we probably parsed it right.
      ASSERT_NEAR(nav_sol.time_distance.time1, 490165, 1);
    }
  }
}

TEST(IcdGroupsParsing, group2) {
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("poslv_grp2.pcapng", "172.16.20.12",
                                  5600, network::ProtocolType::UDP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));
  ASSERT_NE(payload.data, nullptr);

  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);
  auto it = pos_parser.begin();
  while (it != pos_parser.end() && it->getId() != applanix_driver::icd::group::GROUP_ID_2) {
    ++it;
  }
  ASSERT_TRUE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x02);  // Just double checking

  auto vehicle_nav_rms = it->as<applanix_driver::icd::group::VehicleNavRms>();

  ASSERT_EQ(vehicle_nav_rms.header.id, 0x02);
  ASSERT_EQ(vehicle_nav_rms.padding[0], 0);
  ASSERT_EQ(vehicle_nav_rms.padding[1], 0);
}

TEST(IcdGroupsParsing, group3) {
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("poslv_grp3.pcapng", "172.16.20.12",
                                  5600, network::ProtocolType::UDP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));
  ASSERT_NE(payload.data, nullptr);

  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);
  auto it = pos_parser.begin();
  while (it != pos_parser.end() && it->getId() != applanix_driver::icd::group::GROUP_ID_3) {
    ++it;
  }
  ASSERT_TRUE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x03);

  auto primary_gnss_status = it->as<applanix_driver::icd::group::PrimaryGnssStatus>();
  ASSERT_EQ(primary_gnss_status.header.id, 0x03);
  ASSERT_EQ(primary_gnss_status.padding[0], 0);
  ASSERT_EQ(primary_gnss_status.padding[1], 0);
}

TEST(IcdGroupsParsing, group4) {
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("grp4.pcapng", "172.16.20.152",
                                  5606, network::ProtocolType::TCP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));
  ASSERT_NE(payload.data, nullptr);
  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);

  auto it = pos_parser.begin();
  while (it != pos_parser.end() && it->getId() != applanix_driver::icd::group::GROUP_ID_4) {
    ++it;
  }
  ASSERT_TRUE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x04);

  auto imu_data = it->as<applanix_driver::icd::group::TimeTaggedImuData>();
  ASSERT_EQ(imu_data.imu_type, 57);
  ASSERT_EQ(imu_data.data_status, std::byte{0});
  ASSERT_EQ(imu_data.data_rate, std::byte{0b0000'0010});
}

TEST(IcdGroupsParsing, iterator) {
  network::NonOwningBuffer payload{nullptr, 0};
  network::PcapReader pcap_reader("poslv_grp2.pcapng", "172.16.20.12",
                                  5600, network::ProtocolType::UDP);
  ASSERT_TRUE(static_cast<bool>(pcap_reader));
  ASSERT_TRUE(pcap_reader.readSingle(&payload));
  ASSERT_NE(payload.data, nullptr);

  applanix_driver::icd::GroupVector
      pos_parser(reinterpret_cast<const std::byte *>(payload.data), payload.length);
  ASSERT_TRUE(pos_parser.isValid());

  auto it = pos_parser.begin();
  ASSERT_FALSE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x07);

  ++it;
  ASSERT_FALSE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x08);

  ++it;
  ASSERT_TRUE(it->isSupported());
  ASSERT_EQ(it->getId(), 0x02);

  ++it;
  ASSERT_EQ(it, pos_parser.end());
}
