#pragma once

#include <arpa/inet.h>
#include <atomic>
#include <cstddef>
#include <functional>
#include <list>
#include <memory>
#include <optional>
#include <string>
#include <thread>
#include <unordered_map>

#include "applanix_driver/icd/group.h"
#include "applanix_driver/icd/group_parser.h"
#include "applanix_driver/icd/message.h"
#include "applanix_driver/icd/message_parser.h"
#include "applanix_driver/icd/icd_vector.h"
#include "network/ip_client.h"

namespace applanix_driver {

/**
 * This client is meant to connect to products communicating through Applanix's Group outputs described in
 * PUBS-ICD-003759.
 */
class LvClient {
 public:
  using GroupCallback = std::function<void(const icd::group::GroupParser &)>;
  using MessageCallback = std::function<void(const icd::message::MessageParser &)>;
  struct unsupported_callback_error : public std::runtime_error {
    unsupported_callback_error() : std::runtime_error("Unable to dispatch callback, need to add dispatch in"
                                                      "LvClient::callbackDispatch") {}
  };

  LvClient() = delete;
  LvClient(const std::string &lv_ip, unsigned int lv_port, bool enable_display_port = false,
           std::optional<std::string> pcap_filename = std::nullopt);
  LvClient(const LvClient &) = delete;
  LvClient &operator=(const LvClient &) = delete;
  ~LvClient();

  util::Status start();
  void stop();

  std::list<GroupCallback>::iterator registerCallback(icd::group::Id group, const GroupCallback &callback);
  std::list<MessageCallback>::iterator registerCallback(icd::message::Id msg, const MessageCallback &callback);

  void unregisterCallback(icd::group::Id group, const std::list<GroupCallback>::iterator &);
  void unregisterCallback(icd::message::Id msg, const std::list<MessageCallback>::iterator &);

 private:
  std::unique_ptr<std::thread> tcp_thread_, udp_thread_;
  std::atomic_bool keep_running_;

  std::unique_ptr<network::IpClient> tcp_client_, udp_client_;

  std::unordered_map<icd::group::Id, std::list<GroupCallback>> group_callbacks_;
  std::unordered_map<icd::message::Id, std::list<MessageCallback>> msg_callbacks_;

  void runTcpConnection();
  void grabAndParseTcp();

  void runUdpConnection();
  void grabAndParseUdp();
};

}  // namespace applanix_driver
