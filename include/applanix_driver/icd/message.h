#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include "applanix_driver/math.h"

//
namespace applanix_driver::icd::message {

constexpr std::array<std::byte, 4> START = {std::byte('$'), std::byte('M'), std::byte('S'), std::byte('G')};
constexpr std::array<std::byte, 2> END = {std::byte('$'), std::byte('#')};

using Id = std::uint16_t;

constexpr Id ID_GENERAL_PARAMS = 20;

constexpr auto MESSAGE_ID_20 = ID_GENERAL_PARAMS;

#pragma pack(push, 1)
struct Header {
  Id id;
  std::uint16_t byte_count;
  std::uint16_t transaction_num;
  static constexpr size_t OFFSET = START.size();
};

struct Footer {
  std::uint16_t checksum;
};

struct LeverArms {
  Xyzf reference_to_imu;
  Xyzf reference_to_primary_gnss;
  Xyzf reference_to_aux_1;
  Xyzf reference_to_aux_2;
};

struct MountingAngles {
  Xyzf reference_to_imu;
  Xyzf reference_to_vehicle;
};

struct GeneralParams {
  Header header;
  std::uint8_t time_types;
  std::uint8_t distance_type;
  std::uint8_t auto_start;
  LeverArms lever_arms;
  MountingAngles mounting_angles;
  std::uint8_t multipath;
  std::uint16_t padding;
  std::uint16_t checksum;

  [[nodiscard]] bool isInMultipathEnvironment() const {
    return static_cast<bool>(multipath & 0x01U);
  }
};
using Message20 = GeneralParams;

#pragma pack(pop)

}  // namespace applanix_driver::icd::message
