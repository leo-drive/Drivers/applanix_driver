#pragma once

#include "applanix_driver/icd/group_parser.h"
#include "applanix_driver/icd/message_parser.h"

namespace applanix_driver::icd {

template<typename IcdParser>
class IcdVector : public ParserInterface {
 public:
  IcdVector(const std::byte *data, const std::size_t length) {
    setData(data, length);
  }
  IcdVector() : IcdVector(nullptr, 0) {}

  void setData(const std::byte *data, std::size_t length) override {
    data_ = data;
    length_ = length;
  }

  [[nodiscard]] bool isValid() const override {
    IcdParser parser(data_, length_);
    return parser.isValid();
  }

  [[nodiscard]] bool isSupported() const override {
    IcdParser parser(data_, length_);
    return parser.isValid();
  }

  // Forward declaration so we can have all our methods together up top
  class Iterator;

  Iterator begin() {
    return Iterator(data_, length_);
  }
  Iterator end() {
    return this->begin().invalidate();
  }

  class Iterator {
   public:
    friend class IcdVector<IcdParser>;
    using iterator_category = std::forward_iterator_tag;
    using value_type = IcdParser;
    using difference_type = int;
    using pointer = value_type *;
    using reference = value_type &;

    Iterator(const std::byte *const data_begin, std::size_t length) :
        data_begin_(data_begin),
        data_(data_begin_),
        length_(length),
        current_offset_(0),
        current_icd_data(data_begin, length) {}

    reference operator*() {
      if (current_offset_ >= length_) throw std::out_of_range("Tried to access element past end of buffer.");
      return current_icd_data;
    }

    pointer operator->() {
      if (current_offset_ >= length_) throw std::out_of_range("Tried to access element past end of buffer.");
      return &current_icd_data;
    }

    Iterator &operator++() {
      if (current_offset_ >= length_) return *this;

      current_offset_ += current_icd_data.getHeader().byte_count + current_icd_data.getStartBytesSize();
      if (current_offset_ >= length_) {
        invalidate();
      } else {
        data_ = data_begin_ + current_offset_;
        current_icd_data = IcdParser(data_, length_ - current_offset_);
      }
      return *this;
    }

    bool operator==(const Iterator &rhs) const {
      return current_offset_ == rhs.current_offset_ &&
          data_ == rhs.data_ &&
          data_begin_ == rhs.data_begin_ &&
          length_ == rhs.length_;
    }

    bool operator!=(const Iterator &rhs) const {
      return !(*this == rhs);
    }

   private:
    const std::byte *const data_begin_;
    const std::byte *data_;
    const std::size_t length_;
    std::size_t current_offset_;
    IcdParser current_icd_data;

    Iterator &invalidate() {
      current_offset_ = length_;
      data_ = nullptr;
      return *this;
    }
  };

 private:
};

template
class IcdVector<message::MessageParser>;
using MessageVector = IcdVector<message::MessageParser>;
template
class IcdVector<group::GroupParser>;
using GroupVector = IcdVector<group::GroupParser>;

}  // namespace applanix_driver::icd
