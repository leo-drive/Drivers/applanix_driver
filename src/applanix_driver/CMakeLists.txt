add_subdirectory(gsof)
add_subdirectory(icd)

appl_cc_lib(
    NAME
      ${PROJECT_NAME}
    SRCS
      lv_client.cpp
      lvx_client.cpp
    INC
      include
    COPTS
      ${COMMON_COMPILE_FLAGS}
    DEPS
      appl::gsof
      appl::icd
      appl::network
      appl::util
      Threads::Threads
)

install(
  DIRECTORY ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/
  DESTINATION include/${PROJECT_NAME})
