#include "applanix_driver/icd/group_parser.h"

#include "applanix_driver/icd/group.h"

namespace applanix_driver::icd::group {

const GroupParser::GroupIdSet GroupParser::supported_groups_ = {
    VEHICLE_NAV_SOLUTION,
    VEHICLE_NAV_RMS,
    PRIMARY_GNSS_STATUS,
    TIME_TAGGED_IMU_DATA,
    EVENT_1,
    EVENT_2,
    EVENT_3,
    EVENT_4,
    EVENT_5,
    EVENT_6,
    REFERENCE_NAV_SOLUTION,
    REFERENCE_NAV_RMS
};

const std::size_t GroupParser::GROUP_DATA_OFFSET = START.size() + sizeof(Header);

GroupParser::GroupParser(const std::byte *data, const std::size_t length) : data_(data), length_(length) { }

GroupParser::GroupParser(const GroupParser &rhs) :
    data_(rhs.data_),
    length_(rhs.length_) {
}

void GroupParser::setData(const std::byte *data, std::size_t length) {
  data_ = data;
  length_ = length;
}

Id GroupParser::getId() const {
  return getHeader().id;
}

Header GroupParser::getHeader() const {
  Header header;
  std::memcpy(&header, data_ + Header::OFFSET, sizeof(header));
  return header;
}

bool GroupParser::isValid() const {
  // Just check if there is at least 1 valid message at the start
  if (std::memcmp(START.data(), data_, START.size()) != 0) return false;

  Header header = getHeader();

  // Out of bounds check
  if (header.byte_count + GROUP_DATA_OFFSET > length_) return false;

  // TODO(Andre) Also compute checksum

  const size_t end_idx = GROUP_DATA_OFFSET + (header.byte_count - END.size());
  return std::memcmp(END.data(), data_ + end_idx, END.size()) == 0;
}

bool GroupParser::isSupported() const {
  return isValid() && supported_groups_.count(getId()) > 0;
}

std::size_t GroupParser::getStartBytesSize() const {
  return START.size() + sizeof(Header);
}

}  // namespace applanix_driver::icd::group
