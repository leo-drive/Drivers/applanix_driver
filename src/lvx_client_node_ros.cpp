#include <rclcpp/rclcpp.hpp>

#include "applanix_driver_ros/lvx_client_ros.hpp"

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<applanix_driver_ros::LvxClientRos>());
  rclcpp::shutdown();

  return 0;
}
