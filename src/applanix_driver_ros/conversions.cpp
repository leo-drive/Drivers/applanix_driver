#include "applanix_driver_ros/conversions.h"

#include <GeographicLib/UTMUPS.hpp>
#include <rclcpp/logging.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "applanix_driver/math.h"

namespace applanix_driver_ros
{

using applanix_driver::icd::group::TimeDistance;
using applanix_driver::icd::group::NavSolution;
using applanix_driver::icd::group::NavRms;
using applanix_driver::icd::group::PrimaryGnssStatus;
using applanix_driver::deg2rad;

geometry_msgs::msg::TransformStamped toTransformStamped(
  const std_msgs::msg::Header & header,
  const decltype(geometry_msgs::msg::TransformStamped::child_frame_id) & child_frame_id,
  const geometry_msgs::msg::Pose & pose)
{
  geometry_msgs::msg::TransformStamped t;
  t.header = header;
  t.child_frame_id = child_frame_id;
  t.transform.translation.x = pose.position.x;
  t.transform.translation.y = pose.position.y;
  t.transform.translation.z = pose.position.z;
  t.transform.rotation = pose.orientation;
  return t;
}

nav_msgs::msg::Odometry toOdometry(
  const applanix_driver::icd::group::NavSolution & nav_solution,
  const GeographicLib::LocalCartesian & local_cartesian)
{
  if (nav_solution.time_distance.getTime1Source() != TimeDistance::TimeSource::UTC) {
    // If not UTC, we can't know the leap seconds for GPS-UTC.
    RCLCPP_WARN_ONCE(
      rclcpp::get_logger(
        "rclcpp"), "Time source is not UTC, unable to convert to local time.");
  }

  nav_msgs::msg::Odometry odom;
  odom.header.stamp = rclcpp::Time(nav_solution.time_distance.time1 * 1e9, RCL_STEADY_TIME);

  applanix_driver::Enud enu;
  const auto & lla = nav_solution.lla;
  local_cartesian.Forward(lla.latitude, lla.longitude, lla.altitude, enu.east, enu.north, enu.up);

  // Convert to NED while we're at it
  applanix_driver::Nedd ned(enu);
  odom.pose.pose.position = toPoint(ned);

  tf2::Quaternion q;
  q.setEuler(
    deg2rad(nav_solution.attitude.heading),
    deg2rad(nav_solution.attitude.pitch),
    deg2rad(nav_solution.attitude.roll));
  odom.pose.pose.orientation = tf2::toMsg(q);

  odom.twist.twist.linear = toVector(nav_solution.velocity);
  odom.twist.twist.angular = toVector(nav_solution.angular_rate);

  odom.twist.twist.angular.x = deg2rad(odom.twist.twist.angular.x);
  odom.twist.twist.angular.y = deg2rad(odom.twist.twist.angular.y);
  odom.twist.twist.angular.z = deg2rad(odom.twist.twist.angular.z);

  odom.pose.covariance[0] = -1;
  odom.twist.covariance[0] = -1;

  return odom;
}

nav_msgs::msg::Odometry toOdometry(
  const applanix_driver::icd::group::NavSolution & nav_solution,
  const GeographicLib::LocalCartesian & local_cartesian,
  const applanix_driver::icd::group::NavRms & nav_rms)
{
  nav_msgs::msg::Odometry odom = toOdometry(nav_solution, local_cartesian);

  odom.pose.covariance[0] = std::pow(nav_rms.position_rms.north, 2);
  odom.pose.covariance[7] = std::pow(nav_rms.position_rms.east, 2);
  odom.pose.covariance[14] = std::pow(nav_rms.position_rms.down, 2);
  odom.pose.covariance[21] = std::pow(deg2rad(nav_rms.attitude_rms.roll), 2);
  odom.pose.covariance[28] = std::pow(deg2rad(nav_rms.attitude_rms.pitch), 2);
  odom.pose.covariance[35] = std::pow(deg2rad(nav_rms.attitude_rms.heading), 2);

  odom.twist.covariance[0] = std::pow(nav_rms.velocity_rms.north, 2);
  odom.twist.covariance[7] = std::pow(nav_rms.velocity_rms.east, 2);
  odom.twist.covariance[14] = std::pow(nav_rms.velocity_rms.down, 2);

  return odom;
}

constexpr std::int64_t k_to_nano = 1e9;
constexpr std::int64_t k_milli_to_nano = 1e6;
constexpr std::int64_t k_gps_seconds_in_week = 60 * 60 * 24 * 7;

rclcpp::Time toRosTimeOfTheWeek(const applanix_driver::gsof::GpsTime & gps_time)
{
  return rclcpp::Time(gps_time.time_msec * k_milli_to_nano, RCL_STEADY_TIME);
}

rclcpp::Time toRosTimeGpsEpoch(const applanix_driver::gsof::GpsTime & gps_time)
{
  // GpsTime has week as 16 bit so we don't need to take week rollover into account
  const std::int64_t gps_nanoseconds_since_epoch =
    (k_gps_seconds_in_week * static_cast<std::int64_t>(gps_time.week) * k_to_nano) +
    (k_milli_to_nano * gps_time.time_msec);
  return rclcpp::Time(gps_nanoseconds_since_epoch, RCL_STEADY_TIME);
}

rclcpp::Time toRosTimeOfTheWeek(const applanix_driver::icd::group::TimeDistance & time_distance)
{
  using TimeSource = applanix_driver::icd::group::TimeDistance::TimeSource;
  double seconds = 0;
  if (time_distance.getTime1Source() == TimeSource::GPS) {
    seconds = time_distance.time1;
  } else if (time_distance.getTime2Source() == TimeSource::GPS) {
    seconds = time_distance.time2;
  } else {
    throw std::logic_error(
            "Unable to get GPS time of the week because none of the time fields in TimeDistance are set"
            "to TimeSource::GPS");
  }

  const std::int64_t gps_nanoseconds_of_the_week = seconds * k_to_nano;
  return rclcpp::Time(gps_nanoseconds_of_the_week, RCL_STEADY_TIME);
}

rclcpp::Time toRosTimeUtc(const applanix_driver::icd::group::TimeDistance & time_distance)
{
  using TimeSource = applanix_driver::icd::group::TimeDistance::TimeSource;
  double seconds = 0;
  if (time_distance.getTime1Source() == TimeSource::UTC) {
    seconds = time_distance.time1;
  } else if (time_distance.getTime2Source() == TimeSource::UTC) {
    seconds = time_distance.time2;
  } else {
    throw std::logic_error(
            "Unable to get UTC time because none of the time fields in TimeDistance are set"
            "to TimeSource::UTC");
  }

  const std::int64_t nanoseconds_utc = seconds * k_to_nano;
  return rclcpp::Time(nanoseconds_utc, RCL_STEADY_TIME);
}

nav_msgs::msg::Odometry toOdometry(
  const applanix_driver::gsof::InsSolution & ins_solution,
  const GeographicLib::LocalCartesian & local_cartesian)
{
  nav_msgs::msg::Odometry odom;
  odom.header.stamp = toRosTimeOfTheWeek(ins_solution.gps_time);

  applanix_driver::Enud enu;
  const auto & lla = ins_solution.lla;
  local_cartesian.Forward(lla.latitude, lla.longitude, lla.altitude, enu.east, enu.north, enu.up);

  // Convert to NED while we're at it
  applanix_driver::Nedd ned(enu);
  odom.pose.pose.position = toPoint(ned);

  tf2::Quaternion q;
  q.setEuler(
    deg2rad(ins_solution.attitude.heading),
    deg2rad(ins_solution.attitude.pitch),
    deg2rad(ins_solution.attitude.roll));
  odom.pose.pose.orientation = tf2::toMsg(q);

  odom.twist.twist.linear = toVector(ins_solution.velocity);
  odom.twist.twist.angular = toVector(ins_solution.angular_rate);

  odom.twist.twist.angular.x = deg2rad(odom.twist.twist.angular.x);
  odom.twist.twist.angular.y = deg2rad(odom.twist.twist.angular.y);
  odom.twist.twist.angular.z = deg2rad(odom.twist.twist.angular.z);

  odom.pose.covariance[0] = -1;
  odom.twist.covariance[0] = -1;

  return odom;
}

nav_msgs::msg::Odometry toOdometry(
  const applanix_driver::gsof::InsSolution & ins_solution,
  const GeographicLib::LocalCartesian & local_cartesian,
  const applanix_driver::gsof::InsSolutionRms & ins_solution_rms)
{
  nav_msgs::msg::Odometry odom = toOdometry(ins_solution, local_cartesian);

  odom.pose.covariance[0] = std::pow(ins_solution_rms.position_rms.north, 2);
  odom.pose.covariance[7] = std::pow(ins_solution_rms.position_rms.east, 2);
  odom.pose.covariance[14] = std::pow(ins_solution_rms.position_rms.down, 2);
  odom.pose.covariance[21] = std::pow(deg2rad(ins_solution_rms.attitude_rms.roll), 2);
  odom.pose.covariance[28] = std::pow(deg2rad(ins_solution_rms.attitude_rms.pitch), 2);
  odom.pose.covariance[35] = std::pow(deg2rad(ins_solution_rms.attitude_rms.heading), 2);

  odom.twist.covariance[0] = std::pow(ins_solution_rms.velocity_rms.north, 2);
  odom.twist.covariance[7] = std::pow(ins_solution_rms.velocity_rms.east, 2);
  odom.twist.covariance[14] = std::pow(ins_solution_rms.velocity_rms.down, 2);

  return odom;
}

nav_msgs::msg::Odometry toUtm(const applanix_driver::icd::group::NavSolution & nav_solution)
{
  nav_msgs::msg::Odometry odom;
  odom.header.stamp = toRosTimeOfTheWeek(nav_solution.time_distance);

  const auto & lla = nav_solution.lla;
  int zone;
  bool is_northern_hemisphere;
  double x, y;

  GeographicLib::UTMUPS::Forward(lla.latitude, lla.longitude, zone, is_northern_hemisphere, x, y);

  odom.pose.pose.position.x = x;
  odom.pose.pose.position.y = y;
  odom.pose.pose.position.z = lla.altitude;
  tf2::Quaternion q;
  q.setEuler(
    deg2rad(nav_solution.attitude.heading),
    deg2rad(nav_solution.attitude.pitch),
    deg2rad(nav_solution.attitude.roll));
  odom.pose.pose.orientation = tf2::toMsg(q);

  odom.twist.twist.linear = toVector(nav_solution.velocity);
  odom.twist.twist.angular = toVector(nav_solution.angular_rate);

  odom.twist.twist.angular.x = deg2rad(odom.twist.twist.angular.x);
  odom.twist.twist.angular.y = deg2rad(odom.twist.twist.angular.y);
  odom.twist.twist.angular.z = deg2rad(odom.twist.twist.angular.z);

  odom.pose.covariance[0] = -1;
  odom.twist.covariance[0] = -1;

  return odom;
}

nav_msgs::msg::Odometry toUtm(
  const applanix_driver::icd::group::NavSolution & nav_solution,
  const applanix_driver::icd::group::NavRms & nav_rms)
{
  nav_msgs::msg::Odometry odom = toUtm(nav_solution);

  odom.pose.covariance[0] = std::pow(nav_rms.position_rms.north, 2);
  odom.pose.covariance[7] = std::pow(nav_rms.position_rms.east, 2);
  odom.pose.covariance[14] = std::pow(nav_rms.position_rms.down, 2);
  odom.pose.covariance[21] = std::pow(deg2rad(nav_rms.attitude_rms.roll), 2);
  odom.pose.covariance[28] = std::pow(deg2rad(nav_rms.attitude_rms.pitch), 2);
  odom.pose.covariance[35] = std::pow(deg2rad(nav_rms.attitude_rms.heading), 2);

  odom.twist.covariance[0] = std::pow(nav_rms.velocity_rms.north, 2);
  odom.twist.covariance[7] = std::pow(nav_rms.velocity_rms.east, 2);
  odom.twist.covariance[14] = std::pow(nav_rms.velocity_rms.down, 2);

  return odom;
}

sensor_msgs::msg::NavSatFix toNavSatFix(
  const applanix_driver::icd::group::NavSolution & nav_solution,
  const applanix_driver::icd::group::PrimaryGnssStatus & gnss_status)
{
  namespace group = applanix_driver::icd::group;
  using group::GnssConstellation;

  sensor_msgs::msg::NavSatFix nav_sat_fix;
  nav_sat_fix.header.stamp = toRosTimeOfTheWeek(nav_solution.time_distance);
  nav_sat_fix.latitude = nav_solution.lla.latitude;
  nav_sat_fix.longitude = nav_solution.lla.longitude;
  nav_sat_fix.altitude = nav_solution.lla.altitude;

  nav_sat_fix.position_covariance[0] = -1;  // means unknown

  bool is_sat_augmented = false;
  for (const auto & channel_status : gnss_status.channel_status) {
    if (channel_status.getConstellation() == GnssConstellation::GPS) {
      nav_sat_fix.status.service |= sensor_msgs::msg::NavSatStatus::SERVICE_GPS;
    } else if (channel_status.getConstellation() == GnssConstellation::GLONASS) {
      nav_sat_fix.status.service |= sensor_msgs::msg::NavSatStatus::SERVICE_GLONASS;
    } else if (channel_status.getConstellation() == GnssConstellation::BEIDOU) {
      nav_sat_fix.status.service |= sensor_msgs::msg::NavSatStatus::SERVICE_COMPASS;
    } else if (channel_status.getConstellation() == GnssConstellation::SBAS ||
      channel_status.getConstellation() == GnssConstellation::QZSS)
    {
      // Not supported by ROS message
      is_sat_augmented = true;
    } else if (channel_status.getConstellation() == GnssConstellation::GALILEO) {
      nav_sat_fix.status.service |= sensor_msgs::msg::NavSatStatus::SERVICE_GALILEO;
    }
  }

  if (gnss_status.nav_solution_status == group::NAV_STATUS_UNKNOWN ||
    gnss_status.nav_solution_status == group::NAV_STATUS_NO_DATA)
  {
    nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_NO_FIX;
  } else if (gnss_status.nav_solution_status == group::NAV_STATUS_HORZ_CA ||
    gnss_status.nav_solution_status == group::NAV_STATUS_3D_CA ||
    gnss_status.nav_solution_status == group::NAV_STATUS_PCODE)
  {
    nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_FIX;
  } else if (gnss_status.nav_solution_status >= group::NAV_STATUS_HORZ_DGPS &&
    gnss_status.nav_solution_status <= group::NAV_STATUS_OMNISTAR_G4_PLUS)
  {
    nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_GBAS_FIX;
  }

  // Ground based augmentation is probably better than satellite based augmentation, don't replace if set
  if (is_sat_augmented &&
    nav_sat_fix.status.status != sensor_msgs::msg::NavSatStatus::STATUS_GBAS_FIX)
  {
    nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_SBAS_FIX;
  }

  return nav_sat_fix;
}

sensor_msgs::msg::NavSatFix toNavSatFix(
  const applanix_driver::icd::group::NavSolution & nav_solution,
  const applanix_driver::icd::group::PrimaryGnssStatus & gnss_status,
  const applanix_driver::icd::group::NavRms & nav_rms)
{
  sensor_msgs::msg::NavSatFix nav_sat_fix = toNavSatFix(nav_solution, gnss_status);

  // XXX ROS NavSatFix says position covariance is in ENU
  nav_sat_fix.position_covariance[0] = std::pow(nav_rms.position_rms.east, 2);
  nav_sat_fix.position_covariance[4] = std::pow(nav_rms.position_rms.north, 2);
  nav_sat_fix.position_covariance[8] = std::pow(nav_rms.position_rms.down, 2);

  nav_sat_fix.position_covariance_type =
    sensor_msgs::msg::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

  return nav_sat_fix;
}

sensor_msgs::msg::NavSatFix toNavSatFix(const applanix_driver::gsof::InsSolution & ins_solution)
{
  sensor_msgs::msg::NavSatFix nav_sat_fix;
  nav_sat_fix.latitude = ins_solution.lla.latitude;
  nav_sat_fix.longitude = ins_solution.lla.longitude;
  nav_sat_fix.altitude = ins_solution.lla.altitude;

  nav_sat_fix.position_covariance[0] = -1;  // Means unknown

  using GnssStatus = applanix_driver::gsof::Status::GnssStatus;

  switch (ins_solution.status.getGnssStatus()) {
    case GnssStatus::GNSS_SPS_MODE:
    case GnssStatus::GPS_PPS_MODE:
    case GnssStatus::DIRECT_GEOREFERENCING_MODE:
    case GnssStatus::FLOAT_RTK:
      nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_FIX;
      break;
    case GnssStatus::DIFFERENTIAL_GPS_SPS:
    case GnssStatus::FIXED_RTK_MODE:
      nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_GBAS_FIX;
      break;
    case GnssStatus::FIX_NOT_AVAILABLE:
    case GnssStatus::UNKNOWN:
      nav_sat_fix.status.status = sensor_msgs::msg::NavSatStatus::STATUS_NO_FIX;
      break;
  }

  // Note(Andre) There are 3 GSOF messages giving more details about which space vehicles are being used, we can use
  //  those to populate this field if we really need it. However, the enumerations in the sensor_msgs package don't
  //  cover nearly as many constellations Trimble products can receive from.
  nav_sat_fix.status.service = sensor_msgs::msg::NavSatStatus::SERVICE_GPS;

  return nav_sat_fix;
}

sensor_msgs::msg::NavSatFix toNavSatFix(
  const applanix_driver::gsof::InsSolution & ins_solution,
  const applanix_driver::gsof::InsSolutionRms & covariance)
{
  sensor_msgs::msg::NavSatFix nav_sat_fix = toNavSatFix(ins_solution);

  // XXX ROS NavSatFix says position covariance is in ENU
  nav_sat_fix.position_covariance[0] = std::pow(covariance.position_rms.east, 2);
  nav_sat_fix.position_covariance[4] = std::pow(covariance.position_rms.north, 2);
  nav_sat_fix.position_covariance[8] = std::pow(covariance.position_rms.down, 2);

  nav_sat_fix.position_covariance_type =
    sensor_msgs::msg::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

  return nav_sat_fix;
}

applanix_msgs::msg::EventGroup toRosMessage(const applanix_driver::icd::group::Event & event)
{
  applanix_msgs::msg::EventGroup ros_event;
  ros_event.header.stamp = toRosTimeOfTheWeek(event.time_distance);
  ros_event.time_distance = toRosMessage(event.time_distance);
  ros_event.pulse_number = event.event_pulse_number;
  return ros_event;
}

applanix_msgs::msg::NavigationSolutionGroup1 toRosMessage(
  const applanix_driver::icd::group::NavSolution & nav_solution)
{
  applanix_msgs::msg::NavigationSolutionGroup1 sol;
  sol.header.stamp = toRosTimeOfTheWeek(nav_solution.time_distance);
  sol.time_distance = toRosMessage(nav_solution.time_distance);
  sol.lla = toRosMessage(nav_solution.lla);
  sol.velocity = toRosMessage(nav_solution.velocity);
  sol.roll = nav_solution.attitude.roll;
  sol.pitch = nav_solution.attitude.pitch;
  sol.heading = nav_solution.attitude.heading;
  sol.wander_angle = nav_solution.wander_angle;
  sol.track_angle = nav_solution.track_angle;
  sol.speed = nav_solution.speed;
  sol.ang_rate_long = nav_solution.angular_rate.x;
  sol.ang_rate_trans = nav_solution.angular_rate.y;
  sol.ang_rate_down = nav_solution.angular_rate.z;
  sol.acc_long = nav_solution.acceleration.x;
  sol.acc_trans = nav_solution.acceleration.y;
  sol.acc_down = nav_solution.acceleration.z;
  return sol;
}

applanix_msgs::msg::NavigationPerformanceGroup2 toRosMessage(
  const applanix_driver::icd::group::NavRms & nav_rms)
{
  applanix_msgs::msg::NavigationPerformanceGroup2 ros_rms;
  ros_rms.header.stamp = toRosTimeOfTheWeek(nav_rms.time_distance);
  ros_rms.time_distance = toRosMessage(nav_rms.time_distance);
  ros_rms.pos_rms_error = toRosMessage(nav_rms.position_rms);
  ros_rms.vel_rms_error = toRosMessage(nav_rms.velocity_rms);
  ros_rms.attitude_rms_error_roll = nav_rms.attitude_rms.roll;
  ros_rms.attitude_rms_error_pitch = nav_rms.attitude_rms.pitch;
  ros_rms.attitude_rms_error_heading = nav_rms.attitude_rms.heading;
  ros_rms.ellipsoid_error_semi_major = nav_rms.error_ellipsoid_semi_major;
  ros_rms.ellipsoid_error_semi_minor = nav_rms.error_ellipsoid_semi_minor;
  ros_rms.ellipsoid_error_orientation = nav_rms.error_ellipsoid_orientation;
  return ros_rms;
}

applanix_msgs::msg::TimeDistanceGroup toRosMessage(
  const applanix_driver::icd::group::TimeDistance & time_distance)
{
  applanix_msgs::msg::TimeDistanceGroup ros_time_dist;
  ros_time_dist.time1 = time_distance.time1;
  ros_time_dist.time2 = time_distance.time2;
  ros_time_dist.time_type = time_distance.time_type;
  ros_time_dist.distance_tag = time_distance.distance_tag;
  ros_time_dist.distance_type = time_distance.distance_type;
  return ros_time_dist;
}

applanix_msgs::msg::TimeTaggedImuDataGroup4 toRosMessage(
  const applanix_driver::icd::group::TimeTaggedImuData & imu_data)
{
  applanix_msgs::msg::TimeTaggedImuDataGroup4 ros_imu_data;
  ros_imu_data.header.stamp = toRosTimeOfTheWeek(imu_data.time_distance);
  ros_imu_data.time_distance = toRosMessage(imu_data.time_distance);

  std::transform(
    imu_data.imu_data.begin(), imu_data.imu_data.end(), ros_imu_data.imu_data.begin(),
    [](std::byte byte) -> std::uint8_t {
      return static_cast<std::uint8_t>(byte);
    });

  ros_imu_data.data_status = static_cast<std::uint8_t>(imu_data.data_status);
  ros_imu_data.imu_type = static_cast<std::uint8_t>(imu_data.imu_type);
  ros_imu_data.data_rate = static_cast<std::uint8_t>(imu_data.data_rate);
  ros_imu_data.imu_status = static_cast<std::uint16_t>(imu_data.imu_status);
  return ros_imu_data;
}

applanix_msgs::msg::GpsTimeGsof toRosMessage(const applanix_driver::gsof::GpsTime & gps_time)
{
  applanix_msgs::msg::GpsTimeGsof gps_time_gsof;
  gps_time_gsof.time = gps_time.time_msec;
  gps_time_gsof.week = gps_time.week;
  return gps_time_gsof;
}

applanix_msgs::msg::StatusGsof toRosMessage(const applanix_driver::gsof::Status & status)
{
  applanix_msgs::msg::StatusGsof status_gsof;
  status_gsof.gnss = status.gnss;
  status_gsof.imu_alignment = status.imu_alignment;
  return status_gsof;
}

applanix_msgs::msg::NavigationSolutionGsof49 toRosMessage(
  const applanix_driver::gsof::InsSolution & ins_solution)
{
  applanix_msgs::msg::NavigationSolutionGsof49 sol;
  sol.header.stamp = toRosTimeOfTheWeek(ins_solution.gps_time);
  sol.gps_time = toRosMessage(ins_solution.gps_time);
  sol.status = toRosMessage(ins_solution.status);
  sol.lla = toRosMessage(ins_solution.lla);
  sol.velocity = toRosMessage(ins_solution.velocity);
  sol.total_speed = ins_solution.total_speed;
  sol.roll = ins_solution.attitude.roll;
  sol.pitch = ins_solution.attitude.pitch;
  sol.heading = ins_solution.attitude.heading;
  sol.track_angle = ins_solution.track_angle;
  sol.ang_rate_long = ins_solution.angular_rate.roll;
  sol.ang_rate_trans = ins_solution.angular_rate.pitch;
  sol.ang_rate_down = ins_solution.angular_rate.heading;
  sol.acc_long = ins_solution.acceleration.x;
  sol.acc_trans = ins_solution.acceleration.y;
  sol.acc_down = ins_solution.acceleration.z;

  return sol;
}

applanix_msgs::msg::NavigationPerformanceGsof50 toRosMessage(
  const applanix_driver::gsof::InsSolutionRms & ins_solution_rms)
{
  applanix_msgs::msg::NavigationPerformanceGsof50 ros_rms;
  ros_rms.header.stamp = toRosTimeOfTheWeek(ins_solution_rms.gps_time);
  ros_rms.gps_time = toRosMessage(ins_solution_rms.gps_time);
  ros_rms.status = toRosMessage(ins_solution_rms.status);
  ros_rms.pos_rms_error = toRosMessage(ins_solution_rms.position_rms);
  ros_rms.vel_rms_error = toRosMessage(ins_solution_rms.velocity_rms);
  ros_rms.attitude_rms_error_roll = ins_solution_rms.attitude_rms.roll;
  ros_rms.attitude_rms_error_pitch = ins_solution_rms.attitude_rms.pitch;
  ros_rms.attitude_rms_error_heading = ins_solution_rms.attitude_rms.heading;
  return ros_rms;
}

applanix_msgs::msg::RawDmiDataGsof52 toRosMessage(
  const applanix_driver::gsof::DmiRawData & dmi_raw_data)
{
  applanix_msgs::msg::RawDmiDataGsof52 raw_dmi;
  raw_dmi.header.stamp = toRosTimeOfTheWeek(dmi_raw_data.gps_time);
  raw_dmi.gps_time = toRosMessage(dmi_raw_data.gps_time);
  raw_dmi.num_raw_meas = dmi_raw_data.num_raw_meas;
  raw_dmi.time_offset = raw_dmi.time_offset;
  raw_dmi.abs_dist_count = raw_dmi.abs_dist_count;
  raw_dmi.ud_dist_count = raw_dmi.ud_dist_count;
  return raw_dmi;
}

}  // namespace applanix_driver_ros
