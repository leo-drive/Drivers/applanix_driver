#include <rclcpp/rclcpp.hpp>

#include "applanix_driver_ros/lv_client_ros.h"

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<applanix_driver_ros::LvClientRos>());
  rclcpp::shutdown();

  return 0;
}
